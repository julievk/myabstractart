$(document).ready(function() {

    drawBird();


function drawBird(){
    var canvas = document.getElementById("canvas");
    if(canvas.getContext){
        var context = canvas.getContext('2d');

        //ellipse body
        let deg = 20;
        let rad = deg * (Math.PI / 180.0);
        
        context.fillStyle = "#ffaaaa";
        context.beginPath();
        context.ellipse(266.6, 196.2, 86, 45, rad, 0, 2 * Math.PI);
        context.fill();
        context.closePath();
        
        //legs
        context.fillStyle = "#ffaaaa";
        context.beginPath();
        context.fillRect(228.6, 235.2, 17, 177);
        
        context.closePath();

        context.fillStyle = "#ffaaaa";
        context.beginPath();
        context.fillRect(284.79997, 235.80001, 17, 177);
        context.closePath();

        //tail
        context.fillStyle = "#ffaaaa";
        context.beginPath();
        context.moveTo(375, 260);
        context.lineTo(260, 240);
        context.lineTo(325, 175);
        context.fill();
        context.closePath();

        
        //neck
        let deg2 = 350;
        let rad2 = deg2 * (Math.PI / 180.0);
        context.fillStyle = "#ffaaaa";
        context.beginPath();
        context.rotate(rad2);
        context.fillRect(170.6, 112.2, 9, 71);
        context.closePath();

        //head
        context.fillStyle = "#ffaaaa";
        context.strokeStyle = "#ffaaaa";
        context.beginPath();
        //had to adjust position a little
        context.ellipse(170.6, 95.2, 43, 23, 0, 0, 2 * Math.PI);
        context.fill();
        context.stroke();
        context.closePath();


        //beak
        context.fillStyle = "#ffaaaa";
        context.beginPath();
        context.moveTo(130, 90);
        context.lineTo(115, 120);
        context.lineTo(145, 115);
        context.fill();
        context.closePath();


        //eye
        context.fillStyle = "#ffffff";
        context.strokeStyle = "#ffffff";
        context.beginPath();
        context.arc(155.6, 88.2, 10, 0, 2 * Math.PI);
        context.fill();
        context.stroke();
        context.closePath();

        context.fillStyle = "#3f77af";
        context.beginPath();
        context.arc(155, 88.2, 5, 0, 2 * Math.PI);
        context.fill();
        context.closePath();

        //function to change eyecolor
        $('#canvas').hover( function() {
            context.fillStyle = "red";
            context.fill();
        },
        function() {
            context.fillStyle = "#3f77af";
            context.fill();         
    });

       
    }
}


});